﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LabWeek5
{
    internal class Register
    {
        public string Name { get; set; }
        public string SecondName { get; set; }
        public string Email { get; set; }

        public Register(string name, string secondName, string email)
        {
            Name = name;
            SecondName = secondName;
            Email = email;
        }
    }
}
