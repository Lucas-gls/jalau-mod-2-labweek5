﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LabWeek5
{
    public partial class Form4 : Form
    {
        public Form4()
        {
            InitializeComponent();
            InitializeView();
        }

        public void InitializeView()
        {
            listView1.View = View.Details;
            listView1.Columns.Add("Name", -2, HorizontalAlignment.Left);
            listView1.Columns.Add("Surname", -2, HorizontalAlignment.Left);
            listView1.Columns.Add("Email", -2, HorizontalAlignment.Left);

            int kdkoad = Registers.registers.Count();
            listView2.Items.Add($"{kdkoad}");
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            listView1.Items.Clear();
            listView2.Items.Clear();

            var registersDescendente = Registers.registers.OrderByDescending(e => e.Email).ToList();

            foreach (var register in registersDescendente)
            {
                listView1.Items.Add(new ListViewItem(new[] { register.Name, register.SecondName, register.Email }));
            }

            int countRegisters = Registers.registers.Count();
            listView2.Items.Add($"{countRegisters}");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            listView1.Items.Clear();
            listView2.Items.Clear();

            var registersDescendente = Registers.registers.OrderBy(e => e.Email).ToList();

            foreach (var register in registersDescendente)
            {
                listView1.Items.Add(new ListViewItem(new[] { register.Name, register.SecondName, register.Email }));
            }

            int countRegisters = Registers.registers.Count();
            listView2.Items.Add($"{countRegisters}");
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            listView1.Items.Clear();
            listView2.Items.Clear();

            string emailSearched = textBox1.Text;

            var searchs = Registers.registers.Where(r => r.Email == emailSearched).ToList();

            foreach (var search in searchs)
            {
                listView1.Items.Add(new ListViewItem(new[] { search.Name, search.SecondName, search.Email }));
            }

            int countRegisters = searchs.Count();
            listView2.Items.Add($"{countRegisters}");
        }

        private void listView2_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
