﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LabWeek5
{
    public partial class Form3 : Form
    {
        public Form3()
        {
            InitializeComponent();
            InitializeListView();

        }

        public void InitializeListView()
        {
            listView1.View = View.Details;
            listView1.Columns.Add("Name", -2, HorizontalAlignment.Left);
            listView1.Columns.Add("Surname", -2, HorizontalAlignment.Left);
            listView1.Columns.Add("Email", -2, HorizontalAlignment.Left);

        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {

            listView1.Items.Clear();

            foreach (var register in Registers.registers)
            {
                listView1.Items.Add((new ListViewItem(new[] { register.Name, register.SecondName, register.Email })));
            }

        }

        private void Form3_Load(object sender, EventArgs e)
        {

        }
    }
}
