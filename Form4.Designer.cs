﻿namespace LabWeek5
{
    partial class Form4
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            textBox1 = new TextBox();
            label1 = new Label();
            listView1 = new ListView();
            button1 = new Button();
            button2 = new Button();
            listView2 = new ListView();
            label2 = new Label();
            button3 = new Button();
            SuspendLayout();
            // 
            // textBox1
            // 
            textBox1.Location = new Point(173, 79);
            textBox1.Name = "textBox1";
            textBox1.Size = new Size(353, 23);
            textBox1.TabIndex = 0;
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.BackColor = Color.FromArgb(64, 0, 64);
            label1.Font = new Font("Segoe UI Black", 9.75F, FontStyle.Bold, GraphicsUnit.Point, 0);
            label1.ForeColor = Color.White;
            label1.Location = new Point(173, 59);
            label1.Name = "label1";
            label1.Size = new Size(107, 17);
            label1.TabIndex = 1;
            label1.Text = "Search By Email";
            label1.Click += label1_Click;
            // 
            // listView1
            // 
            listView1.Location = new Point(173, 119);
            listView1.Name = "listView1";
            listView1.Size = new Size(438, 214);
            listView1.TabIndex = 2;
            listView1.UseCompatibleStateImageBehavior = false;
            // 
            // button1
            // 
            button1.Font = new Font("Segoe UI Black", 8.75F, FontStyle.Bold);
            button1.ForeColor = Color.FromArgb(64, 0, 64);
            button1.Location = new Point(173, 349);
            button1.Name = "button1";
            button1.Size = new Size(206, 23);
            button1.TabIndex = 3;
            button1.Text = "Ascendente ";
            button1.UseVisualStyleBackColor = true;
            button1.Click += button1_Click;
            // 
            // button2
            // 
            button2.Font = new Font("Segoe UI Black", 8.75F, FontStyle.Bold);
            button2.ForeColor = Color.FromArgb(64, 0, 64);
            button2.Location = new Point(397, 349);
            button2.Name = "button2";
            button2.Size = new Size(214, 23);
            button2.TabIndex = 4;
            button2.Text = "Descendente";
            button2.UseVisualStyleBackColor = true;
            button2.Click += button2_Click;
            // 
            // listView2
            // 
            listView2.Font = new Font("Segoe UI", 12F);
            listView2.Location = new Point(656, 79);
            listView2.Name = "listView2";
            listView2.Size = new Size(74, 67);
            listView2.TabIndex = 5;
            listView2.UseCompatibleStateImageBehavior = false;
            listView2.SelectedIndexChanged += listView2_SelectedIndexChanged;
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Font = new Font("Segoe UI Black", 9.75F, FontStyle.Bold, GraphicsUnit.Point, 0);
            label2.ForeColor = Color.White;
            label2.Location = new Point(662, 59);
            label2.Name = "label2";
            label2.Size = new Size(59, 17);
            label2.TabIndex = 6;
            label2.Text = "Amount";
            label2.Click += label2_Click;
            // 
            // button3
            // 
            button3.Location = new Point(536, 79);
            button3.Name = "button3";
            button3.Size = new Size(75, 23);
            button3.TabIndex = 7;
            button3.Text = "Search";
            button3.UseVisualStyleBackColor = true;
            button3.Click += button3_Click;
            // 
            // Form4
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            BackColor = Color.FromArgb(64, 0, 64);
            ClientSize = new Size(800, 450);
            Controls.Add(button3);
            Controls.Add(label2);
            Controls.Add(listView2);
            Controls.Add(button2);
            Controls.Add(button1);
            Controls.Add(listView1);
            Controls.Add(label1);
            Controls.Add(textBox1);
            Name = "Form4";
            Text = "Form4";
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private TextBox textBox1;
        private Label label1;
        private ListView listView1;
        private Button button1;
        private Button button2;
        private ListView listView2;
        private Label label2;
        private Button button3;
    }
}